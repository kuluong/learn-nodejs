"use strict";
const AccessService = require("../service/access.service");
const {
  ResponseOk,
  ResponseCreated,
  SuccessResponse,
} = require("../core/success.response");
class AccessController {
  handlerRefreshToken = async (req, res, next) => {
    //new SuccessResponse({
    //   message: "Get token success",
    //   metaData: await AccessService.handlerRefreshToken(req.body.refreshToken),
    // }).send(res);

    //version 2 fixed, no need accessToken
    new SuccessResponse({
      message: "Get token success",
      metaData: await AccessService.handlerRefreshTokenV2({
        refreshToken: req.refreshToken,
        user: req.user,
        keyStore: req.keyStore,
      }),
    }).send(res);
  };
  logout = async (req, res, next) => {
    new SuccessResponse({
      message: "logout success",
      metaData: await AccessService.logout(req.keyStore),
    }).send(res);
  };

  login = async (req, res, next) => {
    new SuccessResponse({
      message: "success",
      metaData: await AccessService.login(req.body),
    }).send(res);
  };

  signUp = async (req, res, next) => {
    new ResponseCreated({
      message: "Registered OK!",
      metaData: await AccessService.signup(req.body),
      options: {
        limit: "10",
      },
    }).send(res);
  };
}

module.exports = new AccessController();
