"use strict";

const {
  clothing,
  electronic,
  furniture,
  product,
} = require("../../modules/product.model");
const { getSelectData, unGetSelectData } = require("../../utils/index");
const { Types } = require("mongoose");
const findAllDraftsForShop = async ({ query, limit, skip }) => {
  return await queryProduct({
    productSchema: product,
    query,
    limit,
    skip,
    populate: ["product_shop", "name email -_id"],
  });
};
const findAllPublishForShop = async ({ query, limit, skip }) => {
  return await queryProduct({
    productSchema: product,
    query,
    limit,
    skip,
    populate: ["product_shop", "name email -_id"],
  });
};

const publishProductByShop = async ({ product_shop, product_id }) => {
  console.log({ product_shop, product_id });
  const foundShop = await product.findOne({
    product_shop: new Types.ObjectId(product_shop),
    _id: new Types.ObjectId(product_id),
  });

  if (!foundShop) return null;

  foundShop.isDraft = false;
  foundShop.isPublish = true;

  const { modifiedCount } = await foundShop.updateOne(foundShop);

  return modifiedCount;
};
const unPublishProductByShop = async ({ product_shop, product_id }) => {
  console.log({ product_shop, product_id });
  const foundShop = await product.findOne({
    product_shop: new Types.ObjectId(product_shop),
    _id: new Types.ObjectId(product_id),
  });

  if (!foundShop) return null;

  foundShop.isDraft = true;
  foundShop.isPublish = false;

  const { modifiedCount } = await foundShop.updateOne(foundShop);

  return modifiedCount;
};

const searchProductByUser = async ({ keyword }) => {
  const regexSearch = new RegExp(keyword);
  const results = await product
    .find(
      {
        isPublish: true,
        $text: { $search: regexSearch },
      },
      { score: { $meta: "textScore" } }
    )
    .sort({ score: { $meta: "textScore" } })
    .lean();

  const searchResults = {
    length: results.length,
    data: results,
  };
  return searchResults;
};

const findAllProductByUser = async ({ limit, sort, page, filter, select }) => {
  console.log(filter);
  const skip = (page - 1) * limit;
  const sortBy = sort === "ctime" ? { _id: -1 } : { _id: 1 };
  const query = await product
    .find(filter)
    .sort(sortBy)
    .skip(skip)
    .limit(limit)
    .select(getSelectData(select))
    .lean();

  return {
    length: query.length,
    data: query,
  };
};
const findOneProductByUser = async ({ product_id, unSelect }) => {
  return await product.findById(product_id).select(unGetSelectData(unSelect)).lean();
};

const updateProductById = async ({ productId, payload, model, isNew = true }) => {
  console.log({ productId, payload, model });
  return await model.findByIdAndUpdate(productId, payload, {
    new: isNew,
  });
};

const queryProduct = async ({ productSchema, query, limit, skip, populate }) => {
  return await productSchema
    .find(query)
    .populate(populate[0], populate[1])
    .sort({ updateAt: -1 })
    .skip(skip)
    .limit(limit)
    .lean()
    .exec();
};
module.exports = {
  findAllDraftsForShop,
  findAllPublishForShop,
  publishProductByShop,
  unPublishProductByShop,
  searchProductByUser,
  findAllProductByUser,
  findOneProductByUser,
  updateProductById,
};
