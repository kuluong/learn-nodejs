"use strict";

const express = require("express");
const ProductController = require("../../controller/product.controller");
const { authenticationV2 } = require("../../auth/authUtils");
const asyncHandler = require("../../helpers/asyncHandler");

const router = express.Router();

router.get(
  "/search/:keyword",
  asyncHandler(ProductController.getListSearchProductByUser)
);
router.get("/", asyncHandler(ProductController.getFindAllProductByUser));
router.get("/:product_id", asyncHandler(ProductController.getFindOneProductByUser));

router.use(authenticationV2);

router.post("", asyncHandler(ProductController.createProduct));

router.patch("/:productId", asyncHandler(ProductController.updateProductById));

router.post("/publish/:id", asyncHandler(ProductController.publishProductByShop));
router.post("/unpublish/:id", asyncHandler(ProductController.unPublishProductByShop));

//Query//
router.get("/drafts/all", asyncHandler(ProductController.getAllDraftsForShop));
router.get("/publish/all", asyncHandler(ProductController.getAllPublishForShop));

module.exports = router;
