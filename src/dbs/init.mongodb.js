"use strict";

const mongoose = require("mongoose");
const { countConnect } = require("../helpers/check.connect");
const {
  db: { host, name, port },
} = require("../configs/config.mogodb");

const connStr = `mongodb+srv://luongtrandev:${process.env.DB_PASSWORD}@cluster0.vbws2sx.mongodb.net/`;

class Database {
  constructor() {
    this.connect();
  }

  //connect
  connect(type = "mongodb") {
    if (1 === 1) {
      mongoose.set("debug", true);
      mongoose.set("debug", { color: true });
    }
    mongoose
      .connect(connStr, { maxPoolSize: 50 }) // maxPoolSize: number of connects to db
      .then((_) => {
        console.log(`connected Mongodb Success`, countConnect());
      })
      .catch((err) => console.log(err));
  }
  //create a file instance
  static getInstance() {
    //check database has instance yet
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }
}
const instanceMongoDb = Database.getInstance();

module.exports = instanceMongoDb;
