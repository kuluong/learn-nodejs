"use strict";

const _ = require("lodash");

const getInfoData = ({ fields = [], object = {} }) => {
  return _.pick(object, fields);
};

// ['a','b'] = {a:1,b:1}
const getSelectData = (select = []) => {
  return Object.fromEntries(select.map((el) => [el, 1]));
};
// ['a','b'] = {a:0,b:0}
const unGetSelectData = (select = []) => {
  return Object.fromEntries(select.map((el) => [el, 0]));
};

const removeUndefinedOrNullInObject = (object) => {
  Object.keys(object).forEach((k) => {
    if (object[k] === "undefined" || object[k] === null) {
      delete object[k];
    }
  });
  return object;
};

//làm phẳng 1 object --- ex: {a:1,b:{c:2,d:null}} => {a:1,b.c:2}
const nestedObjectParser = (obj) => {
  const final = {};
  Object.keys(obj || {}).forEach((k) => {
    if (typeof obj[k] === "object" && !Array.isArray(obj[k])) {
      const response = nestedObjectParser(obj[k]);
      Object.keys(response).forEach((a) => {
        final[`${k}.${a}`] = response[a];
      });
    } else {
      final[k] = obj[k];
    }
  });
  return final;
};

module.exports = {
  getInfoData,
  getSelectData,
  unGetSelectData,
  removeUndefinedOrNullInObject,
  nestedObjectParser,
};
