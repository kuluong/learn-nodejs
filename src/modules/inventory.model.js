"use strict";

const { Schema, model, Types } = require("mongoose");

const DOCUMENT_NAME = "Inventory";
const COLLECTION_NAME = "Inventories";
//Declare the Schema of the Mongo Model

const inventorySchema = new Schema(
  {
    inven_productId: { type: Schema.Types.ObjectId, ref: "Product" },
    inven_location: { type: String, default: "unKnow" },
    inven_stock: { type: Number, require: true }, // so luong hang ton kho
    inven_shopId: { type: Schema.Types.ObjectId, ref: "Shop" },
    inven_reservations: { type: Array, default: [] }, // đặt hàng trước, có nghĩa bỏ sản phẩm vào giỏ hàng
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

//export the model

module.exports = model(DOCUMENT_NAME, inventorySchema);
