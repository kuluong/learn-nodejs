"use strict";
const keyTokenModel = require("./../modules/keyToken.model");
const mongoose = require("mongoose");
const { Types } = require("mongoose");
class KeyTokenService {
  static createKeyToken = async ({ userId, privateKey, publicKey, refreshToken }) => {
    try {
      //################# level 0 ##############
      //create keyToken for collection keys on database
      // const token = await keyTokenModel.create({
      //   user: userId,
      //   privateKey,
      //   publicKey,
      // });
      // return token ? token.publicKey : null;

      //############# level xx ##################

      const filter = { user: userId };
      const update = { publicKey, privateKey, refreshTokensUsed: [], refreshToken };
      const options = { new: true, upsert: true };
      const tokens = await keyTokenModel.findOneAndUpdate(filter, update, options);

      return tokens ? tokens.publicKey : null;
    } catch (error) {
      return error;
    }
  };

  static findByUserId = async (userId) => {
    return await keyTokenModel.findOne({ user: new mongoose.Types.ObjectId(userId) });
  };
  static removeKeyById = async (id) => {
    return await keyTokenModel.deleteOne({ _id: id }).lean();
  };

  static findByRefreshTokenUsed = async (refreshToken) => {
    return await keyTokenModel.findOne({ refreshTokensUsed: refreshToken }).lean();
  };

  static findByRefreshToken = async ({ refreshToken }) => {
    console.log("FR", refreshToken);
    return await keyTokenModel.findOne({ refreshToken: refreshToken });
  };
  static deleteKeyById = async (userId) => {
    return await keyTokenModel.deleteOne({ user: userId }).lean();
  };
}
module.exports = KeyTokenService;
