"use strict";
const JWT = require("jsonwebtoken");
const asyncHandler = require("../helpers/asyncHandler");
const { AuthFailureError, NotFoundError } = require("../core/error.response");
const KeyTokenService = require("../service/keyToken.service");

const HEADER = {
  API_KEY: "x-api-key",
  CLIENT_ID: "x-client-id",
  AUTHORIZATION: "authorization",
  REFRESHTOKEN: "x-rtoken-id",
};

const createTokensPair = (payload, publicKey, privateKey) => {
  try {
    //access token
    const accessToken = JWT.sign(payload, publicKey, {
      expiresIn: "2 days",
    });

    // refreshToken
    const refreshToken = JWT.sign(payload, privateKey, {
      expiresIn: "7 days",
    });

    //verify token
    JWT.verify(accessToken, publicKey, (err, decode) => {
      if (err) {
        console.log(`error verify`, err);
      } else {
        console.log(`decode verify`, decode);
      }
    });
    return { accessToken, refreshToken };
  } catch (error) {
    return error;
  }
};

const authentication = asyncHandler(async (req, res, next) => {
  /**
   * check userId in headers exist
   * check keyStore with userId
   * get accessToken from headers
   *  verify JWT
   * check userID with userId in decodeUser has the same
   * assign keyStore to req.keyStore
   * ok all => return next()
   */

  // check userId in headers exist

  const userId = req.headers[HEADER.CLIENT_ID];
  if (!userId) throw new AuthFailureError("Invalid Request");
  //check keyStore with userId
  const keyStore = await KeyTokenService.findByUserId(userId);

  if (!keyStore) throw new NotFoundError("Not found keyStore");
  //get accessToken from headers using bearer Token

  //1. if not used bearToken then
  // const accessToken = req.headers[HEADER.AUTHORIZATION]

  //2. if used bearerToken then
  const bearerToken = req.headers[HEADER.AUTHORIZATION];
  const accessToken = bearerToken.split(" ")[1];
  console.log(accessToken);
  if (!accessToken) throw new AuthFailureError("Invalid Request");

  try {
    //verify JWT
    const decodeUser = JWT.verify(accessToken, keyStore.publicKey);

    console.log(decodeUser);
    //check userID with userId in decodeUser has the same
    if (userId !== decodeUser.userId) throw new AuthFailureError("Invalid userID");

    // assign keyStore to req.keyStore
    req.keyStore = keyStore;
    return next();
  } catch (error) {
    throw error;
  }
});

const authenticationV2 = asyncHandler(async (req, res, next) => {
  /**
   * check userId in headers exist
   * check keyStore with userId
   * get accessToken from headers
   * check accessToken exists
   * check refreshToken in req.headers exists
   * verify JWT
   * check userID with userId in decodeUser has the same
   * assign keyStore,user, refreshToken to req
   * ok all => return next()
   */

  // check userId in headers exist

  const userId = req.headers[HEADER.CLIENT_ID];
  if (!userId) throw new AuthFailureError("Invalid Request");
  //check keyStore with userId
  const keyStore = await KeyTokenService.findByUserId(userId);

  if (!keyStore) throw new NotFoundError("Not found keyStore with userID");

  //get accessToken from headers using bearer Token
  //1. if not used bearToken then
  // const accessToken = req.headers[HEADER.AUTHORIZATION]
  //2. if used bearerToken then
  const bearerToken = req.headers[HEADER.AUTHORIZATION];
  if (!bearerToken) throw new AuthFailureError("Invalid Request");

  //check if has refreshToken in req.headers
  if (req.headers[HEADER.REFRESHTOKEN]) {
    try {
      //verify JWT
      const refreshToken = req.headers[HEADER.REFRESHTOKEN];
      const decodeUser = await JWT.verify(refreshToken, keyStore.privateKey);

      //check userID with userId in decodeUser has the same
      if (userId !== decodeUser.userId) throw new AuthFailureError("Invalid userID");

      // assign keyStore to req.keyStore
      req.keyStore = keyStore;
      req.user = decodeUser;
      req.refreshToken = refreshToken;
      return next();
    } catch (error) {
      throw error;
    }
  }
  // check if has accessToken in req.headers
  const accessToken = bearerToken.split(" ")[1];

  try {
    //verify token
    const decodeUser = await verifyJWT(accessToken, keyStore.publicKey);
    console.log(decodeUser.userId);
    // check userId has the same decodeUser.userId
    console.log(userId);
    if (userId !== decodeUser.userId) throw new AuthFailureError("Invalid userID");
    // assign keyStore,user  to req
    req.keyStore = keyStore;
    req.user = decodeUser;
    return next();
  } catch (error) {
    throw error;
  }
});

const verifyJWT = async (token, keySecret) => {
  return JWT.verify(token, keySecret);
};

module.exports = {
  createTokensPair,
  authentication,
  authenticationV2,
  verifyJWT,
};
