"use strict";

const StatusCode = {
  OK: 200,
  CREATED: 201,
};

const ReasonStatusCode = {
  OK: "Success",
  CREATED: "Created",
};
class SuccessResponse {
  constructor({
    message,
    statusCode = StatusCode.OK,
    reasonStatusCode = ReasonStatusCode.OK,
    metaData = {},
  }) {
    this.message = !message ? reasonStatusCode : message;
    this.statusCode = statusCode;
    this.metaData = metaData;
  }
  send(res, headers = {}) {
    if (this.statusCode === 200) {
      return res.status(this.statusCode).json({
        data: this.metaData,
      });
    }
    return res.status(this.statusCode).json({
      message: this.message,
      data: this.metaData,
    });
  }
}

class ResponseOk extends SuccessResponse {
  constructor({ message, metaData }) {
    super({ message, metaData });
  }
}

class ResponseCreated extends SuccessResponse {
  constructor({
    options = {},
    message,
    statusCode = StatusCode.CREATED,
    reasonStatusCode = ReasonStatusCode.CREATED,
    metaData,
  }) {
    super({ message, statusCode, reasonStatusCode, metaData });
    this.options = options;
  }
}

module.exports = {
  ResponseOk,
  ResponseCreated,
  SuccessResponse,
};
