"use strict";
const mongoose = require("mongoose");
const os = require("os");
const process = require("process");

const _SECONDS = 600 * 1000; //600 seconds
// check count connect
const countConnect = () => {
  const numConnect = mongoose.connections.length;
  console.log(`Number of Connections:: ${numConnect}`);
};

// check over load
const CheckOverLoad = () => {
  // monitor every five seconds
  setInterval(() => {
    // get count connections on saver
    const numConnections = mongoose.connections.length;
    //get cores of cpu
    const numCores = os.cpus().length;
    //get memory used
    const memoryUsage = process.memoryUsage().rss;

    //example maximum number of connections based on number of cores
    const maximumConnects = numCores * 5;
    // console.log(`Active connections is ${numConnections}`);
    // console.log(`MemoryUsage is ${memoryUsage / 1024 / 1024} MB`);

    // check if numConnection > maximumConnect then do something

    if (numConnections > maximumConnects) {
      console.log("Connections overload detected!");
      //do something: notify.send(...)
    }
  }, _SECONDS); //monitor every five seconds
};

module.exports = {
  countConnect,
  CheckOverLoad,
};
