## Necessary packages for the project

#### express

```
npm i express --save-dev
```

#### morgan

```
npm i morgan --save-dev
```

### helmet

```
npm i helmet --save-dev
```

### compression

```
npm i compression --save-dev
```

### connect to database using mongoose

```
npm i mongoose
```

### dotenv

```
npm i dotenv
```

### bcrypt

```
npm i bcrypt --save
```

### crypto

```
npm i crypto --save
```

### jsonWebToken

```
npm i jsonwebtoken --save
```

### lodash

```
 npm i -g npm
 npm i --save lodash
```
