"use strict";
const { default: mongoose } = require("mongoose");
const { model, Schema } = require("mongoose");
const slugify = require("slugify");
const DOCUMENT_NAME = "Product";
const COLLECTION_NAME = "Products";

// define the product type
const productSchema = new Schema(
  {
    product_name: { type: String, require: [true, "product_name not empty"] },
    product_thumb: { type: String, require: [true, "product_thumb not empty"] },
    product_description: String,
    product_slug: { type: String },
    product_price: { type: Number, require: [true, "product_price not empty"] },
    product_quantity: { type: Number, require: [true, "product_quantity not empty"] },
    product_type: {
      type: String,
      require: [true, "product_type not empty"],
      enum: ["Electronics", "Clothing", "Furniture"],
    },
    product_shop: { type: Schema.Types.ObjectId, ref: "Shop" },
    product_attributes: { type: Schema.Types.Mixed, required: true }, // type kieu hon hop
    //more
    product_ratingAverage: {
      type: Number,
      default: 4.5,
      min: [1, "Rating must be above 1.0"],
      max: [5, "Rating must be above 5.0"],
      set: (val) => Math.round(val * 10) / 10, // ex: 4.34444 => 4.3
    },
    product_variations: { type: Array, default: [] },
    isDraft: { type: Boolean, default: true, index: true, select: false }, // bản nháp
    isPublish: { type: Boolean, default: false, index: true, select: false },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);
//create index for search

productSchema.index({ product_name: "text", product_description: "text" });

//Document middleware: runs before .save() .create() ...

productSchema.pre("save", async function (next) {
  this.product_slug = slugify(this.product_name, { lower: true });
  return next();
});

const clothingSchema = new Schema(
  {
    brand: { type: String, require: true },
    size: String,
    material: String,

    product_shop: {
      type: Schema.Types.ObjectId,
      ref: "Shop",
    },
  },
  {
    timestamps: true,
    collection: "Clothes",
  }
);

const electronicSchema = new Schema(
  {
    manufacturer: { type: String, require: true },
    model: String,
    color: String,
    product_shop: {
      type: Schema.Types.ObjectId,
      ref: "Shop",
    },
  },
  {
    timestamps: true,
    collection: "electronics",
  }
);

const furnitureSchema = new Schema(
  {
    brand: { type: String, require: true },
    size: String,
    material: String,

    product_shop: {
      type: Schema.Types.ObjectId,
      ref: "Shop",
    },
  },
  {
    timestamps: true,
    collection: "furnitures",
  }
);

module.exports = {
  product: model(DOCUMENT_NAME, productSchema),
  electronic: model("Electronics", electronicSchema),
  clothing: model("Clothing", clothingSchema),
  furniture: model("Furniture", furnitureSchema),
};
