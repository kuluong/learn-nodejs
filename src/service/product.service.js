"use strict";

const { clothing, electronic, product } = require("../modules/product.model");
const { BadRequestError } = require("../core/error.response");

//define basic product class
class Product {
  constructor({
    product_name,
    product_thumb,
    product_description,
    product_price,
    product_quantity,
    product_type,
    product_shop,
    product_attributes,
  }) {
    this.product_name = product_name;
    this.product_thumb = product_thumb;
    this.product_description = product_description;
    this.product_price = product_price;
    this.product_quantity = product_quantity;
    this.product_type = product_type;
    this.product_shop = product_shop;
    this.product_attributes = product_attributes;
  }
  // create new product
  async createProduct(product_id) {
    return await product.create({ ...this, _id: product_id });
  }
}

// define sub-class for different product type Clothing

class Clothing extends Product {
  // overwritten method  createProduct of class product

  async createProduct() {
    //insert clothing to database
    const newClothing = await clothing.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    //check if create fail
    if (!newClothing) throw new BadRequestError("Create clothing error");

    // create product Schema
    const newProduct = await super.createProduct(newClothing._id); // super is Product Class (parent)

    //check if create newProduct fail
    if (!newProduct) throw new BadRequestError("Create newProduct error");
    //if newProduct success then return newProduct
    return newProduct;
  }
}

// define sub-class for different product type Electronics

class Electronic extends Product {
  // overwritten method  createProduct of class product

  async createProduct() {
    const newElectronic = await electronic.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    //check if create fail
    if (!newElectronic) throw new BadRequestError("Create Electronic error");

    // create product Schema
    const newProduct = await super.createProduct(newElectronic._id); // super is Product Class (parent)
    //check if create newProduct fail
    if (!newProduct) throw new BadRequestError("Create newProduct error");
    //if newProduct success then return newProduct
    return newProduct;
  }
}

// define Factory class to create product

class ProductFactory {
  static async createProduct(type, payload) {
    switch (type) {
      case "Electronic":
        return new Electronic(payload).createProduct();

      case "Clothing":
        return new Clothing(payload).createProduct();

      default:
        throw new BadRequestError(`Invalid product Types ${type}`);
    }
  }
}

module.exports = ProductFactory;
