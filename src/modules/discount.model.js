"use strict";

const { Schema, model } = require("mongoose");

const DOCUMENT_NAME = "Discount";
const COLLECTION_NAME = "discounts";
//Declare the Schema of the Mongo Model

const discountSchema = new Schema(
  {
    discount_name: { type: String, require: true },
    discount_description: { type: String, require: true },
    // fixed_amount or percentage
    discount_type: { type: String, default: "fixed_amount" },
    discount_value: { type: Number, require: true },
    discount_code: { type: String, require: true },
    discount_start_date: { type: Date, require: true },
    discount_end_date: { type: Date, require: true },
    // so luong discount duoc ap dung
    discount_max_uses: { type: Number, require: true },
    //so discount da su dung
    discount_uses_count: { type: Number, require: true },
    // user nao da su dung
    discount_users_used: { type: Array, default: [] },
    // so luong cho phep toi da duoc su dung moi user
    discount_max_uses_per_user: { type: Number, required: true },
    //gia tien don hang toi thieu de su dung voucher
    discount_min_order_value: { type: Number, require: true },
    // discount nay la cua shop nao
    discount_shopId: { type: Schema.Types.ObjectId, ref: "Shop" },
    //trang thai cua discount
    discount_is_active: { type: Boolean, default: true },
    discount_applies_to: { type: String, default: true, enum: ["all", "specific"] },
    // so san pham duoc ap dung
    discount_product_ids: { type: String, default: [] },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

//export the model

module.exports = model(DOCUMENT_NAME, discountSchema);
