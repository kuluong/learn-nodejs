const app = require("./src/app");
const PORT = process.env.PORT || 5000;
const server = app.listen(PORT, () => {
  console.log(`server running with Port = ${PORT}`);
});

//close server if user presses Ctrl+C in the terminal
process.on("SIGINT", () => {
  server.close(() => console.log("Exit server Express"));
});
