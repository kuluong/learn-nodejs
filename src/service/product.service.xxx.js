"use strict";

const { clothing, electronic, product, furniture } = require("../modules/product.model");
const { BadRequestError } = require("../core/error.response");
const {
  findAllDraftsForShop,
  findAllPublishForShop,
  publishProductByShop,
  unPublishProductByShop,
  searchProductByUser,
  findAllProductByUser,
  findOneProductByUser,
  updateProductById,
} = require("../modules/repositories/product.repo");

const { removeUndefinedOrNullInObject, nestedObjectParser } = require("../utils");
const { insertInventory } = require("../modules/repositories/inevntory.repo");
//define basic product class
class Product {
  constructor({
    product_name,
    product_thumb,
    product_description,
    product_price,
    product_quantity,
    product_type,
    product_shop,
    product_attributes,
  }) {
    this.product_name = product_name;
    this.product_thumb = product_thumb;
    this.product_description = product_description;
    this.product_price = product_price;
    this.product_quantity = product_quantity;
    this.product_type = product_type;
    this.product_shop = product_shop;
    this.product_attributes = product_attributes;
  }
  // create new product
  async createProduct(product_id) {
    const newProduct = await product.create({ ...this, _id: product_id });
    if (newProduct) {
      //add product_stock in inventory collection
      await insertInventory({
        productId: newProduct._id,
        shopId: this.product_shop,
        stock: this.product_quantity,
      });
    }

    return newProduct;
  }
  // update product
  async updateProduct(productId, payload) {
    return await updateProductById({ productId, payload, model: product });
  }
}

// define sub-class for different product type Clothing

class Clothing extends Product {
  // overwritten method  createProduct of class product

  async createProduct() {
    //insert clothing to database
    const newClothing = await clothing.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    //check if create fail
    if (!newClothing) throw new BadRequestError("Create clothing error");

    // create product Schema
    const newProduct = await super.createProduct(newClothing._id); // super is Product Class (parent)

    //check if create newProduct fail
    if (!newProduct) throw new BadRequestError("Create newProduct error");
    //if newProduct success then return newProduct
    return newProduct;
  }

  async updateProduct(productId) {
    //1.remove attr has null or undefined
    const objectParams = removeUndefinedOrNullInObject(this);
    //2 check xem update ở chỗ nào
    if (objectParams.product_attributes) {
      //update child
      await updateProductById({
        productId,
        payload: nestedObjectParser(objectParams.product_attributes),
        model: clothing,
      });
    }
    // update parent
    const updateProduct = await super.updateProduct(
      productId,
      nestedObjectParser(objectParams)
    );
    if (!updateProduct) throw new BadRequestError("update product error");
    return updateProduct;
  }
}

// define sub-class for different product type Electronics

class Electronic extends Product {
  // overwritten method  createProduct of class product

  async createProduct() {
    const newElectronic = await electronic.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    //check if create fail
    if (!newElectronic) throw new BadRequestError("Create Electronic error");

    // create product Schema
    const newProduct = await super.createProduct(newElectronic._id); // super is Product Class (parent)
    //check if create newProduct fail
    if (!newProduct) throw new BadRequestError("Create newProduct error");
    //if newProduct success then return newProduct
    return newProduct;
  }
}

// define sub-class for different product type Furniture
class Furniture extends Product {
  // overwritten method  createProduct of class product

  async createProduct() {
    //insert clothing to database
    const newFurniture = await furniture.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    //check if create fail
    if (!newFurniture) throw new BadRequestError("Create furniture error");

    // create product Schema
    const newProduct = await super.createProduct(newFurniture._id); // super is Product Class (parent)

    //check if create newProduct fail
    if (!newProduct) throw new BadRequestError("Create newProduct error");
    //if newProduct success then return newProduct
    return newProduct;
  }
}

// define Factory class to create product

class ProductFactory {
  //create an object contain key:class
  static productRegistry = {}; // key-class

  static registerProductType(type, classRef) {
    /**
     * Add key -class into object ProductRegistry
     * example
     * productRegistry.demo = xxx
     * console.log(productRegistry.demo) -> xxx
     */
    ProductFactory.productRegistry[type] = classRef;
  }

  static async createProduct(type, payload) {
    //get class based on Type
    const productClass = ProductFactory.productRegistry[type];
    // if don't productClass then
    if (!productClass) throw new BadRequestError(`Invalid Product Types ${type}`);

    return new productClass(payload).createProduct();
  }
  static async updateProductById(type, productId, payload) {
    //get class based on Type
    const productClass = ProductFactory.productRegistry[type];
    // if don't productClass then
    if (!productClass) throw new BadRequestError(`Invalid Product Types ${type}`);

    return new productClass(payload).updateProduct(productId);
  }

  //PUT//
  static async publishProductByShop({ product_shop, product_id }) {
    return await publishProductByShop({ product_shop, product_id });
  }
  static async unPublishProductByShop({ product_shop, product_id }) {
    return await unPublishProductByShop({ product_shop, product_id });
  }
  //END PUT//

  // QUERY ///
  static async findAllDraftsForShop({ product_shop, limit = 50, skip = 0 }) {
    const query = { product_shop, isDraft: true };
    return await findAllDraftsForShop({ query, limit, skip });
  }

  static async findAllPublishForShop({ product_shop, limit = 50, skip = 0 }) {
    const query = { product_shop, isPublish: true };
    return await findAllPublishForShop({ query, limit, skip });
  }

  static async searchProductByUser({ keyword }) {
    return await searchProductByUser({ keyword });
  }

  static async findAllProductByUser({
    limit = 50,
    sort = "ctime",
    page = 1,
    filter = { isPublish: true },
    select = ["product_name", "product_price", "product_thumb"],
  }) {
    console.log(limit);
    return await findAllProductByUser({ limit, sort, page, filter, select });
  }

  static async findOneProductByUser({ product_id }) {
    return await findOneProductByUser({ product_id, unSelect: ["__v"] });
  }

  //END QUERY //
}

//register product types

ProductFactory.registerProductType("Clothing", Clothing);
ProductFactory.registerProductType("Electronics", Electronic);
ProductFactory.registerProductType("Furniture", Furniture);

module.exports = ProductFactory;
