"use strict";
const shopModel = require("../modules/shop.model");
const bcrypt = require("bcrypt");
const crypto = require("node:crypto");
const KeyTokenService = require("./keyToken.service");
const { createTokensPair, verifyJWT } = require("../auth/authUtils");
const { getInfoData } = require("../utils/index");
const {
  BadRequestError,
  AuthFailureError,
  ForbiddenError,
} = require("../core/error.response");
const { findByEmail } = require("./shop.service");
const RoleShop = {
  SHOP: "SHOP",
  WRITER: "WRITER",
  EDITOR: "EDITOR",
  ADMIN: "ADMIN",
};
class AccessService {
  static signup = async ({ name, password, email, roles }) => {
    //step1: check email of shop exists
    const holderShop = await shopModel.findOne({ email }).lean();
    if (holderShop) {
      throw new BadRequestError("ERROR: Shop already registered!");
    }

    //hash password
    const passwordHash = await bcrypt.hash(password, 10);
    // create account
    const newShop = await shopModel.create({
      name,
      password: passwordHash,
      email,
      roles: [RoleShop.SHOP],
    });

    if (newShop) {
      // created privateKey, publicKey with node:crypto
      const privateKey = crypto.randomBytes(64).toString("hex");
      const publicKey = crypto.randomBytes(64).toString("hex");

      //push public key to database
      const keyStore = await KeyTokenService.createKeyToken({
        userId: newShop._id,
        privateKey,
        publicKey,
      });

      if (!keyStore) {
        throw new BadRequestError("keyStore error");
      }

      // created token pair
      const tokens = await createTokensPair(
        {
          userId: newShop._id,
          email,
        },
        publicKey,
        privateKey
      );
      return {
        data: {
          shop: getInfoData({ fields: ["_id", "name", "email"], object: newShop }),
          tokens,
        },
      };
    }
    return {
      code: 200,
      data: null,
    };
  };
  static login = async ({ email, password, refreshToken = null }) => {
    /*
      1. check email in db exits
      2. match password
      3. create accessToken and refreshToken and save
      4. generate Token
      5. get data return login
    */
    //1.
    const foundShop = await findByEmail({ email });
    if (!foundShop) {
      throw new BadRequestError("Shop not registered");
    }

    //2.
    const match = await bcrypt.compare(password.toString(), foundShop.password);
    if (!match) throw new AuthFailureError("Authentication error");

    //3.
    const privateKey = crypto.randomBytes(64).toString("hex");
    const publicKey = crypto.randomBytes(64).toString("hex");
    //4.
    const tokens = await createTokensPair(
      { userId: foundShop._id, email },
      publicKey,
      privateKey
    );

    await KeyTokenService.createKeyToken({
      userId: foundShop._id,
      privateKey,
      publicKey,
      refreshToken: tokens.refreshToken,
    });

    //5.
    return {
      shop: getInfoData({ fields: ["_id", "name", "email"], object: foundShop }),
      tokens,
    };
  };
  static logout = async (keyStore) => {
    return await KeyTokenService.removeKeyById(keyStore._id);
  };
  static handlerRefreshToken = async (refreshToken) => {
    /**
     *  check this refreshTokensUsed used
     *  if has refreshTokensUsed then ...
     *  --- decode see who
     *  --- delete keyStore
     *  if hasn't refreshTokenUsed then ...
     *  -- find refreshToken
     *  -- verify token with refreshToken
     *  -- check userID exists
     *  -- create new tokens
     *  -- update Schema
     *
     */
    //check xem token da duoc su dung chua

    const foundToken = await KeyTokenService.findByRefreshTokenUsed(refreshToken);

    //neu co token
    if (foundToken) {
      // decode xem may la thang nao
      const { userId, email } = await verifyJWT(refreshToken, foundToken.privateKey);

      //xoa tat ca token trong keyStore
      await KeyTokenService.deleteKeyById(userId);
      throw new ForbiddenError("something wrong happened !! Please reLogin");
    }

    // neu khong co refreshTokensUsed
    // tim kiem refreshToken
    const holderToken = await KeyTokenService.findByRefreshToken({ refreshToken });

    if (!holderToken) throw new AuthFailureError("Shop not registered");

    //verifyToken
    const { userId, email } = await verifyJWT(refreshToken, holderToken.privateKey);
    console.log("first", email);
    //check userId
    const foundShop = await findByEmail({ email });
    if (!foundShop) throw new AuthFailureError("Shop not registered");

    // create 1 cap token moi
    const tokens = await createTokensPair(
      { userId, email },
      holderToken.publicKey,
      holderToken.privateKey
    );
    //update token
    await holderToken.updateOne({
      $set: {
        refreshToken: tokens.refreshToken,
      },
      $addToSet: {
        refreshTokensUsed: refreshToken, // da duoc su dung de lay token moi roi
      },
    });
    return {
      user: { userId, email },
      tokens,
    };
  };
  static handlerRefreshTokenV2 = async ({ keyStore, user, refreshToken }) => {
    /**
     *  check this refreshTokensUsed used
     *  if has refreshTokensUsed then ...
     *  --- decode see who
     *  --- delete keyStore
     *  if hasn't refreshTokenUsed then ...
     *  -- find refreshToken
     *  -- verify token with refreshToken
     *  -- check userID exists
     *  -- create new tokens
     *  -- update Schema
     *
     */
    //check xem token da duoc su dung chua

    const { userId, email } = user;
    if (keyStore.refreshTokensUsed.includes(refreshToken)) {
      await KeyTokenService.deleteKeyById(userId);
      throw new ForbiddenError("something wrong happened !! Please reLogin");
    }

    if (keyStore.refreshToken !== refreshToken)
      throw new AuthFailureError("Shop not registered");

    const foundShop = await findByEmail({ email });
    if (!foundShop) throw new AuthFailureError("Shop not registered");

    // create 1 cap token moi
    const tokens = await createTokensPair(
      { userId, email },
      keyStore.publicKey,
      keyStore.privateKey
    );
    //update token
    await keyStore.updateOne({
      $set: {
        refreshToken: tokens.refreshToken,
      },
      $addToSet: {
        refreshTokensUsed: refreshToken, // da duoc su dung de lay token moi roi
      },
    });
    return {
      user,
      tokens,
    };
  };
}

module.exports = AccessService;
