"use strict ";

const { SuccessResponse } = require("../core/success.response");
// const ProductService = require("../service/product.service");
const ProductService = require("../service/product.service.xxx");
class ProductController {
  createProduct = async (req, res, next) => {
    return new SuccessResponse({
      message: "Create new product success",
      statusCode: 201,
      metaData: await ProductService.createProduct(req.body.product_type, {
        ...req.body,
        product_shop: req.user.userId,
      }),
    }).send(res);
  };

  updateProductById = async (req, res, next) => {
    console.log("111");
    return new SuccessResponse({
      message: "update success",
      metaData: await ProductService.updateProductById(
        req.body.product_type,
        req.params.productId,
        {
          ...req.body,
          product_shop: req.user.userId,
        }
      ),
    }).send(res);
  };

  //PUSH//

  publishProductByShop = async (req, res, next) => {
    return new SuccessResponse({
      message: "Publish product success",
      statusCode: 200,
      metaData: await ProductService.publishProductByShop({
        product_id: req.params.id,
        product_shop: req.user.userId,
      }),
    }).send(res);
  };
  unPublishProductByShop = async (req, res, next) => {
    return new SuccessResponse({
      message: "UnPublish product success",
      statusCode: 200,
      metaData: await ProductService.unPublishProductByShop({
        product_id: req.params.id,
        product_shop: req.user.userId,
      }),
    }).send(res);
  };

  //END PUSH//

  //Query//

  getAllDraftsForShop = async (req, res, next) => {
    return new SuccessResponse({
      message: "Get All Drafts success",
      statusCode: 200,
      metaData: await ProductService.findAllDraftsForShop({
        product_shop: req.user.userId,
      }),
    }).send(res);
  };
  getAllPublishForShop = async (req, res, next) => {
    return new SuccessResponse({
      message: "Get All Publish success",
      statusCode: 200,
      metaData: await ProductService.findAllPublishForShop({
        product_shop: req.user.userId,
      }),
    }).send(res);
  };
  getListSearchProductByUser = async (req, res, next) => {
    console.log("here");
    return new SuccessResponse({
      message: "Get Product success",
      statusCode: 200,
      metaData: await ProductService.searchProductByUser(req.params),
    }).send(res);
  };
  getFindAllProductByUser = async (req, res, next) => {
    return new SuccessResponse({
      message: "Get All Product success",
      statusCode: 200,
      metaData: await ProductService.findAllProductByUser(req.query),
    }).send(res);
  };
  getFindOneProductByUser = async (req, res, next) => {
    return new SuccessResponse({
      message: "Get Product success",
      statusCode: 200,
      metaData: await ProductService.findOneProductByUser({
        product_id: req.params.product_id,
      }),
    }).send(res);
  };
  // End Query //
}

module.exports = new ProductController();
